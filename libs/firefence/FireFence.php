<?php
/**
 * Created by PhpStorm.
 * User: stuart
 * Date: 02/12/2015
 * Time: 14:08
 */

namespace Bryter\FireFence;

use Symfony\Component\HttpFoundation\Request;
use Bryter\FireFence\Helpers\Config;


class FireFence {

	/**
	 * @var Request
	 */
	public $request;

	/**
	 * @var Config
	 */
	public $config;

	/**
	 * FireFence constructor.
	 */
	public function __construct(
		array $allowedClientIPs,
		array $allowedHostIPs,
		array $allowedHostNames,
		array $badIPs,
		array $badReferrers
	) {
		$this->request = Request::createFromGlobals();
		$this->config = new Config(
			$allowedClientIPs,
			$allowedHostIPs,
			$allowedHostNames,
			$badIPs,
			$badReferrers
		);
	}

	/**
	 * @return Request
	 */
	public function getRequest()
	{
		return $this->request;
	}

	/**
	 * @param Request $request
	 */
	public function setRequest(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * @return Config
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * @param Config $config
	 */
	public function setConfig(Config $config)
	{
		$this->config = $config;
	}



//	public function logRequest($uid = 0, Request $request = null) {
//		return false;
//	}
}