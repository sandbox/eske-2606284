<?php
/**
 * Created by PhpStorm.
 * User: stuart
 * Date: 02/12/2015
 * Time: 15:42
 */

namespace Bryter\FireFence\Helpers;


class ClientIP {
	public static function getClientIP() {
		$ip = false;

		if ( isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && !empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) ) $ip = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
		else if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if ( isset($_SERVER['HTTP_CF_CONNECTING_IP']) && !empty($_SERVER['HTTP_CF_CONNECTING_IP']) ) $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
		else if ( isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']) ) $ip = $_SERVER['REMOTE_ADDR'];

		return $ip;
	}

	public static function getCompleteListBetweenTwoIps($ipOne, $ipTwo)
	{
		$range = array();
		$ipMax = 256;
		$ipSeg[0]['cur'] = 0;
		$ipSeg[0]['min'] = 1;
		$ipSeg[0]['max'] = 1;
		$ipSeg[1]['cur'] = 0;
		$ipSeg[1]['min'] = 4;
		$ipSeg[1]['max'] = 10;
		$ipSeg[2]['cur'] = 0;
		$ipSeg[2]['min'] = 0;
		$ipSeg[2]['max'] = 16;
		$ipSeg[3]['cur'] = 0;
		$ipSeg[3]['min'] = 0;
		$ipSeg[3]['max'] = 255;

		$seg3 = true;
		$seg2 = true;
		$seg1 = true;
		$seg0 = true;

		if ($ipSeg[0]['min'] == $ipSeg[0]['max']) {
			$ipSeg[0]['cur'] = $ipSeg[0]['min'];
		} else {
			$ipSeg[0]['cur'] = $ipSeg[0]['min'];
			$seg0 = false;
		}

		if ($ipSeg[1]['min'] == $ipSeg[1]['max']) {
			$ipSeg[1]['cur'] = $ipSeg[1]['min'];
		} else {
			$ipSeg[1]['cur'] = $ipSeg[1]['min'];
			$seg1 = false;
		}

		if ($ipSeg[2]['min'] == $ipSeg[2]['max']) {
			$ipSeg[2]['cur'] = $ipSeg[2]['min'];
		} else {
			$ipSeg[2]['cur'] = $ipSeg[2]['min'];
			$seg2 = false;
		}

		if ($ipSeg[3]['min'] == $ipSeg[3]['max']) {
			$ipSeg[3]['cur'] = $ipSeg[3]['min'];
		} else {
			$ipSeg[3]['cur'] = $ipSeg[3]['min'];
			$seg3 = false;
		}

		/**
		 * Wrong
		 * [0] => 1.4.0.0/17
		 * [1] => 1.10.16.0/20
		 */
		$range = array();

		$s = ip2long($ipOne);
		$e = ip2long($ipTwo);
		for($i = $s; $i<$e+1; $i++)
			$range[] =  long2ip($i);

		return $range;
	}


	public static function cidrToIpList($cidr) {
		$range = array();
		$cidr = explode('/', $cidr);
		$range[0] = long2ip((ip2long($cidr[0])) & ((-1 << (32 - (int)$cidr[1]))));
		$range[1] = long2ip((ip2long($cidr[0])) + pow(2, (32 - (int)$cidr[1])) - 1);

		return $range;
	}

	public static function isCidr($ip)
	{
		if ( strpos ( $ip , '/') !== false) return true;
			else return false;
	}

	public static function isPair($ipOne, $ipTwo) {

		$ipOneArray = explode('.', $ipOne);

		$ipTwoArray = explode('.', $ipTwo);

		if ( end($ipOneArray) == '0' && end($ipTwoArray) == '255' ) return true;
			else return false;
	}
}