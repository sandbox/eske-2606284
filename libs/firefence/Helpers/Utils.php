<?php
/**
 * Created by PhpStorm.
 * User: stuart
 * Date: 01/04/2016
 * Time: 13:53
 */

namespace Bryter\FireFence\Helpers;

use Bryter\FireFence\Helpers\Prowl\Prowl;
use Symfony\Component\HttpFoundation\Request;


class Utils
{
    public static function debug($object, $die = false) {
        echo '<pre>';
        print_r($object);
        echo '</pre>';

        if ($die) die();
    }

    public static function valueInArray($targetValue, $targetArray, $targetArrayKey = null, $caseSensitive = false) {
        if (!$caseSensitive && is_string($targetValue) ) $targetValue = strtolower($targetValue);

        if (is_null($targetArrayKey)) {
            // Reminder !== false as value can be 0 (first in the array)
            return array_search($targetValue, $targetArray);
        } else {
            foreach ($targetArray as $key => $item) {
                if (!$caseSensitive && is_string($targetValue) ) $item[$targetArrayKey] = strtolower($item[$targetArrayKey]);

                if ($item[$targetArrayKey] === $targetValue) {
                    return $key; break;
                }
            }
            // Completed the loop and nothing found
            return false;
        }
    }

    /**
     * stringsWithinTargetString
     *
     * @param array $searchStrings
     * @param string $targetStrings
     * @return bool
     */
    public static function stringsWithinTargetString($searchStrings, $targetStrings, $caseSensitive = false) {
        if ( !is_array( $searchStrings ) ) $searchStrings = array( $searchStrings );

        $stringFound = false;

        // If target is single string
        if (is_string($targetStrings)) {

            if (!$caseSensitive) $targetStrings = strtolower($targetStrings);

            foreach ($searchStrings as $searchString) {
                if (!$caseSensitive) $searchString = strtolower($searchString);

                if (strpos($targetStrings, $searchString) !== false) {
                    $stringFound = true;
                    break;
                }
            }
        } else if (is_array($targetStrings)) { // If target string is array

            foreach($targetStrings as $targetString) {

                if (!$caseSensitive) $targetString = strtolower($targetString);

                foreach ($searchStrings as $searchString) {
                    if (!$caseSensitive) $searchString = strtolower($searchString);

                    if (strpos($targetString, $searchString) !== false) {
                        $stringFound = true;
                        break;
                    }
                }
            }
        }

        return $stringFound;
    }

    public static function array1DOfStringsToTextWithEOL($array, $safe = true) {
        $text = '';

        foreach($array as $item) {
            if ($safe) $item = htmlspecialchars($item);
            $text .= $item . PHP_EOL;
        }

        return $text;
    }

    public static function isValidUrl($url) {
        if ( !preg_match("/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/", $url) )
            return false;
        else return true;
    }

    public static function prowl(
        $applicationTxt = 'FireFence Security Alert',
        $eventTxt = 'Unauthorised Access: ',
        $notificationData = array()
    )
    {

        global $base_url;

        try {
            //https://visqueenbuilding.local/?contact-us?#overlay=admin/config/firefence/settings
            $prowl = new \Bryter\FireFence\Helpers\Prowl\Prowl();
            $prowl->setApiKey(FIREFENCE_APIPROWL_KEY);

            $application = $applicationTxt;
            $event = $eventTxt . $base_url;
            $description = json_encode(
                $notificationData
            );
            $url = $base_url . '/' . FIREFENCE_SETTINGS_URL;
            $priority = -1;

            $message = $prowl->add($application, $event, $priority, $description, $url);

            //self::log('Prowl response.', 'debug', $message);

        } Catch (Exception $e) {
            //self::log('Prowl failed.', 'debug', $message);
        }
    }

    public static function log($message, $type = 'info', $data = null) {
        if (!class_exists('\\Monolog\\Logger')) return false;

        $debugLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'debug.log';
        $errorLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'error.log';
        $warningLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'warning.log';
        $infoLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'info.log';

        // Create the logger
        $logger = new \Monolog\Logger('Utils_Logger');

        switch($type) {
            case 'debug':
                $logger->pushHandler(new \Monolog\Handler\StreamHandler($debugLogPath, \Monolog\Logger::DEBUG));
                break;
            case 'warning':
                $logger->pushHandler(new \Monolog\Handler\StreamHandler($warningLogPath, \Monolog\Logger::WARNING));
                break;
            case 'error':
                $logger->pushHandler(new \Monolog\Handler\StreamHandler($errorLogPath, \Monolog\Logger::ERROR));
                break;
            case 'info':
                $logger->pushHandler(new \Monolog\Handler\StreamHandler($infoLogPath, \Monolog\Logger::INFO));
                break;
        }

        // You can now use your logger
        if (is_null($data)) $logger->addInfo($message);
            else $logger->addInfo($message, $data);
    }

    public static function getAllLogsAsString($type = 'info', $nlToBr = true) {
        if (!class_exists('\\Monolog\\Logger')) return false;

        $debugLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'debug.log';
        $errorLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'error.log';
        $warningLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'warning.log';
        $infoLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'info.log';

        $fileContents = '';

        switch($type) {
            default:
                if (is_file($infoLogPath))
                    $fileContents = file_get_contents($infoLogPath);
                break;
        }

        if ($nlToBr) $fileContents = nl2br($fileContents);

        return $fileContents;
    }

    public static function getLatestLogsAsArray($type = 'info', $limit = 100) {
        $debugLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'debug.log';
        $errorLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'error.log';
        $warningLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'warning.log';
        $infoLogPath = FIREFENCE_LOG_PATH . DIRECTORY_SEPARATOR . 'info.log';

        $dataArray = array();

        switch($type) {
            default:
                if (is_file($infoLogPath))
                    $fileContentString = file_get_contents($infoLogPath);
                    $dataArray = explode("\n", $fileContentString);
                break;
        }

        $itemCount = count($dataArray);
        if ($itemCount>= 100) {
            $arrayStartIndex = ($itemCount - $limit) - 1;
            $dataArray = array_slice( $dataArray, $arrayStartIndex, $limit );
        }

        return $dataArray;
    }

    public static function getLatestLogsAsString($nlToBr = false, $type = 'info', $limit = 100) {
        $logContent = implode(
            PHP_EOL,
            self::getLatestLogsAsArray($type, $limit)
        );

        if ($nlToBr) $logContent = nl2br($logContent);

        return $logContent;
    }

    public static function clearAllLogs() {

        if (is_dir(FIREFENCE_LOG_PATH)) {
            \Utils::deleteDir(FIREFENCE_LOG_PATH);
        }
    }
}