<?php
/**
 * Created by PhpStorm.
 * User: stuart
 * Date: 01/04/2016
 * Time: 13:59
 */

namespace Bryter\FireFence\Helpers;


class Config
{

    public $allowedClientIPs;
    public $allowedHostIPs;
    public $allowedHostNames;
    public $badIPs;
    public $badReferrers;

    /**
     * Config constructor.
     * @param $allowedClientIPs
     * @param $allowedHostIPs
     * @param $allowedHostNames
     * @param $badIPs
     * @param $badReferrers
     */
    public function __construct(
        $allowedClientIPs,
        $allowedHostIPs,
        $allowedHostNames,
        $badIPs,
        $badReferrers
    ) {
        $this->allowedClientIPs = $allowedClientIPs;
        $this->allowedHostIPs = $allowedHostIPs;
        $this->allowedHostNames = $allowedHostNames;
        $this->badIPs = $badIPs;
        $this->badReferrers = $badReferrers;
    }

    /**
     * @return mixed
     */
    public function getAllowedClientIPs()
    {
        return $this->allowedClientIPs;
    }

    /**
     * @param mixed $allowedClientIPs
     */
    public function setAllowedClientIPs($allowedClientIPs)
    {
        $this->allowedClientIPs = $allowedClientIPs;
    }

    /**
     * @return mixed
     */
    public function getAllowedHostIPs()
    {
        return $this->allowedHostIPs;
    }

    /**
     * @param mixed $allowedHostIPs
     */
    public function setAllowedHostIPs($allowedHostIPs)
    {
        $this->allowedHostIPs = $allowedHostIPs;
    }

    /**
     * @return mixed
     */
    public function getAllowedHostNames()
    {
        return $this->allowedHostNames;
    }

    /**
     * @param mixed $allowedHostNames
     */
    public function setAllowedHostNames($allowedHostNames)
    {
        $this->allowedHostNames = $allowedHostNames;
    }

    /**
     * @return mixed
     */
    public function getBadIPs()
    {
        return $this->badIPs;
    }

    /**
     * @param mixed $badIPs
     */
    public function setBadIPs($badIPs)
    {
        $this->badIPs = $badIPs;
    }

    /**
     * @return mixed
     */
    public function getBadReferrers()
    {
        return $this->badReferrers;
    }

    /**
     * @param mixed $badReferrers
     */
    public function setBadReferrers($badReferrers)
    {
        $this->badReferrers = $badReferrers;
    }

}