# FireFence

## Log API
Example: https://visqueenbuilding.local/firefence/api/json/v1/logs/7f48fwjsd9rh/16-04-05/81795ce3dd93fa91f20527522f572647  
The url will produce 100 of the latest log entries in json array format.
- $accessString - api key only known to the site admin
- $dateString - The date of the api request
- $hash - the hash should be made up of md5 as+ds

## History 

### v0.16040401
Firewall logger install to /logs and the new actions are:  
- Settings viewed
- Settings saved
- User login
- Admin route blocked
Log viewer added to the settings form.  
Log viewer only latest 100 logs.  
Log API URL.  
Clear logs added.  
Email Logs added.  
Prowl Lib added.  
Prowl notification function added.
User Login notifications added.  
PHP constants added.

#### Files Added
- logs/*
- libs/helpers/prowl

#### Libraries Added
  
  
### v0.16040101
Bad bot strings array exported to data/badreferrers.json.  
External json config files added.  
Composer libraries have been added.  
Admin settings form now wired up and saving data  

#### Files Added
- data/badreferrers.json
- data/allowedclientips.json
- data/allowedhostips.json
- data/allowedhostsnames.json
- data/badips.json
- libs/firefence/helpers/*

#### Libraries Added
- bryter/firefence
- symfony/http-foundation
- monolog/monolog